import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import ptLocales from './pt.json';
import enLocales from './en.json';

let lng = 'pt';

if (typeof localStorage !== 'undefined') {
  lng = localStorage.getItem('i18nextLng') || 'pt';
}

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      pt: { translations: ptLocales },
      en: { translations: enLocales },
    },
    lng,
    fallbackLng: 'pt',
    debug: false,
    ns: ['translations'],
    defaultNS: 'translations',
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;

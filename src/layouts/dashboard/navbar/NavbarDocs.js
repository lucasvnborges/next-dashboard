import { Stack, Button, Typography } from '@mui/material';
import { DocIllustration } from 'src/assets';

export default function NavbarDocs() {
  return (
    <Stack spacing={3} sx={{ px: 5, pb: 5, mt: 10, width: 1, textAlign: 'center', display: 'block' }}>
      <DocIllustration sx={{ width: 1 }} />

      <div>
        <Typography variant="body2" sx={{ color: 'text.secondary' }}>
          Precisa de ajuda?
          <br /> Entre em contato com o suporte
        </Typography>
      </div>

      <Button variant="contained">Fale Conosco</Button>
    </Stack>
  );
}

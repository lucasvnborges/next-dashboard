import { PATH_DASHBOARD } from 'src/routes/paths';
import SvgIconStyle from 'src/components/SvgIconStyle';

const getIcon = (name) => <SvgIconStyle src={`/icons/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const ICONS = {
  blog: getIcon('ic_blog'),
  cart: getIcon('ic_cart'),
  user: getIcon('ic_user'),
  banking: getIcon('ic_banking'),
  ecommerce: getIcon('ic_ecommerce'),
  analytics: getIcon('ic_analytics'),
  dashboard: getIcon('ic_dashboard'),
  booking: getIcon('ic_booking'),
};

const navConfig = [
  {
    subheader: 'dashboards',
    items: [
      {
        title: 'app',
        path: PATH_DASHBOARD.general.app,
        icon: ICONS.dashboard,
      },
      { title: 'e-commerce', path: PATH_DASHBOARD.general.ecommerce, icon: ICONS.ecommerce },
      { title: 'análise', path: PATH_DASHBOARD.general.analytics, icon: ICONS.analytics },
      { title: 'pagamentos', path: PATH_DASHBOARD.general.banking, icon: ICONS.banking },
      { title: 'reservas', path: PATH_DASHBOARD.general.booking, icon: ICONS.booking },
    ],
  },

  {
    subheader: 'gerenciamento',
    items: [
      {
        title: 'usuário',
        path: PATH_DASHBOARD.user.root,
        icon: ICONS.user,
        children: [
          { title: 'perfil', path: PATH_DASHBOARD.user.profile },
          { title: 'cards', path: PATH_DASHBOARD.user.cards },
          { title: 'lista', path: PATH_DASHBOARD.user.list },
          { title: 'criar', path: PATH_DASHBOARD.user.newUser },
          { title: 'editar', path: PATH_DASHBOARD.user.editById },
          { title: 'detalhes', path: PATH_DASHBOARD.user.account },
        ],
      },

      {
        title: 'e-commerce',
        path: PATH_DASHBOARD.eCommerce.root,
        icon: ICONS.cart,
        children: [],
      },

      {
        title: 'blog',
        path: PATH_DASHBOARD.blog.root,
        icon: ICONS.blog,
        children: [],
      },
    ],
  },
];

export default navConfig;

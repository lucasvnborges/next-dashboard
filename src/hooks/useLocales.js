import { useTranslation } from 'react-i18next';
// @mui
import { enUS, ptBR } from '@mui/material/locale';

const LANGS = [
  {
    label: 'Português',
    value: 'pt',
    systemValue: ptBR,
    icon: 'https://res.cloudinary.com/drj6rtiuz/image/upload/v1670326401/br-flag_zxnwaw_pp5in8.svg',
  },
  {
    label: 'Inglês',
    value: 'en',
    systemValue: enUS,
    icon: 'https://res.cloudinary.com/drj6rtiuz/image/upload/v1670318719/ic_flag_en_lnyxur.svg',
  },
];

export default function useLocales() {
  const { i18n, t: translate } = useTranslation();
  const langStorage = localStorage.getItem('i18nextLng');
  const currentLang = LANGS.find((_lang) => _lang.value === langStorage) || LANGS[1];

  const handleChangeLanguage = (newlang) => {
    i18n.changeLanguage(newlang);
  };

  return {
    onChangeLang: handleChangeLanguage,
    translate,
    currentLang,
    allLang: LANGS,
  };
}

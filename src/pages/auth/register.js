import { capitalCase } from 'change-case';
// next
import NextLink from 'next/link';
// @mui
import { styled } from '@mui/material/styles';
import { Box, Link, Container, Typography, Tooltip } from '@mui/material';
// hooks
import useAuth from 'src/hooks/useAuth';
// routes
import { PATH_AUTH } from 'src/routes/paths';
// guards
import GuestGuard from 'src/guards/GuestGuard';
// components
import Page from 'src/components/Page';
import Image from 'src/components/Image';
// sections
import { RegisterForm } from 'src/sections/auth/register';

const RootStyle = styled('div')(({ theme }) => ({
  backgroundColor: '#fff',
  [theme.breakpoints.up('md')]: {
    display: 'flex',
  },
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(10, 0),

  [theme.breakpoints.down('md')]: {
    padding: theme.spacing(6, 0),
    justifyContent: 'flex-start',
  },
}));

export default function Register() {
  const { method } = useAuth();

  return (
    <GuestGuard>
      <Page title="Register">
        <RootStyle>
          <Container>
            <ContentStyle>
              <Box sx={{ mb: 4, display: 'flex', alignItems: 'center' }}>
                <Box sx={{ flexGrow: 1 }}>
                  <Typography variant="h4" gutterBottom>
                    Cadastre-se gratuitamente
                  </Typography>
                  <Typography sx={{ color: 'text.secondary' }}>Aproveite todas as nossas ferramentas</Typography>
                </Box>
                <Tooltip title={capitalCase(method)}>
                  <>
                    <Image
                      disabledEffect
                      alt={method}
                      src={`https://minimal-assets-api.vercel.app/assets/icons/auth/ic_${method}.png`}
                      sx={{ width: 32, height: 32 }}
                    />
                  </>
                </Tooltip>
              </Box>

              <Typography variant="body2" sx={{ mb: 3 }}>
                Já tem uma conta? {''}
                <NextLink href={PATH_AUTH.login} passHref>
                  <Link variant="subtitle2">Entrar</Link>
                </NextLink>
              </Typography>

              <RegisterForm />

              <Typography variant="body2" align="center" sx={{ color: 'text.secondary', mt: 3 }}>
                Ao me registrar no Next Dashboard, concordo com os&nbsp;
                <Link underline="always" color="text.primary" href="#">
                  Termos de serviço
                </Link>{' '}
                e{' '}
                <Link underline="always" color="text.primary" href="#">
                  Política de Privacidade
                </Link>
                .
              </Typography>
            </ContentStyle>
          </Container>
        </RootStyle>
      </Page>
    </GuestGuard>
  );
}

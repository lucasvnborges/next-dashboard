import { capitalCase } from 'change-case';
import NextLink from 'next/link';
import { styled } from '@mui/material/styles';
import { Box, Stack, Link, Alert, Tooltip, Container, Typography } from '@mui/material';

import useAuth from 'src/hooks/useAuth';
import GuestGuard from 'src/guards/GuestGuard';
import Page from 'src/components/Page';
import Image from 'src/components/Image';

import { PATH_AUTH } from 'src/routes/paths';
import { LoginForm } from 'src/sections/auth/login';

const RootStyle = styled('div')(({ theme }) => ({
  backgroundColor: '#fff',
  [theme.breakpoints.up('md')]: {
    display: 'flex',
  },
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(10, 0),

  [theme.breakpoints.down('md')]: {
    padding: theme.spacing(6, 0),
    justifyContent: 'flex-start',
  },
}));

export default function Login() {
  const { method } = useAuth();

  return (
    <GuestGuard>
      <Page title="Login">
        <RootStyle>
          <Container>
            <ContentStyle>
              <Stack direction="row" alignItems="center" sx={{ mb: 4 }}>
                <Box sx={{ flexGrow: 1 }}>
                  <Typography variant="h4" gutterBottom>
                    Entrar no Next Dashboard
                  </Typography>
                  <Typography sx={{ color: 'text.secondary' }}>Insira os seus dados de login abaixo.</Typography>
                </Box>

                <Tooltip title={capitalCase(method)} placement="right">
                  <>
                    <Image
                      disabledEffect
                      alt={method}
                      src={`https://minimal-assets-api.vercel.app/assets/icons/auth/ic_${method}.png`}
                      sx={{ width: 32, height: 32 }}
                    />
                  </>
                </Tooltip>
              </Stack>

              <Typography variant="body2" sx={{ mb: 3 }}>
                Não tem uma conta?{' '}
                <NextLink href={PATH_AUTH.register} passHref>
                  <Link variant="subtitle2">Cadastre-se</Link>
                </NextLink>
              </Typography>

              <Alert severity="info" sx={{ mb: 3 }}>
                Login teste: <strong>Lucas@hapbit.com.br</strong> / senha:<strong> Juxtapos3</strong>
              </Alert>

              <LoginForm />
            </ContentStyle>
          </Container>
        </RootStyle>
      </Page>
    </GuestGuard>
  );
}

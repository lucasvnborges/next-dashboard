import { paramCase, capitalCase } from 'change-case';
// next
import { useRouter } from 'next/router';
// @mui
import { Container } from '@mui/material';
// routes
import { PATH_DASHBOARD } from 'src/routes/paths';
// hooks
import useSettings from 'src/hooks/useSettings';
// _mock_
import { _userList } from 'src/_mock';
// layouts
import Layout from 'src/layouts';
// components
import Page from 'src/components/Page';
import HeaderBreadcrumbs from 'src/components/HeaderBreadcrumbs';
// sections
import UserNewForm from 'src/sections/@dashboard/user/UserNewForm';

UserEdit.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default function UserEdit() {
  const { themeStretch } = useSettings();

  const { query } = useRouter();

  const { name } = query;

  const currentUser = _userList.find((user) => paramCase(user.name) === name);

  return (
    <Page title="User: Editar usuário">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading="Atualizar informações"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'Perfil', href: PATH_DASHBOARD.user.list },
            { name: capitalCase(name) },
          ]}
        />

        <UserNewForm isEdit currentUser={currentUser} />
      </Container>
    </Page>
  );
}

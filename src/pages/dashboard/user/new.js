// @mui
import { Container } from '@mui/material';
// routes
import { PATH_DASHBOARD } from 'src/routes/paths';
// hooks
import useSettings from 'src/hooks/useSettings';
// layouts
import Layout from 'src/layouts';
// components
import Page from 'src/components/Page';
import HeaderBreadcrumbs from 'src/components/HeaderBreadcrumbs';
// sections
import UserNewForm from 'src/sections/@dashboard/user/UserNewForm';

UserCreate.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default function UserCreate() {
  const { themeStretch } = useSettings();

  return (
    <Page title="User: Cadastro">
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <HeaderBreadcrumbs
          heading="Cadastrar usuário"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'Perfil', href: PATH_DASHBOARD.user.list },
            { name: 'Novo usuário' },
          ]}
        />
        <UserNewForm />
      </Container>
    </Page>
  );
}

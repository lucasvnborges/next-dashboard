import { useTheme } from '@mui/material/styles';
import { Container, Grid, Stack } from '@mui/material';
// hooks
import useAuth from 'src/hooks/useAuth';
import useSettings from 'src/hooks/useSettings';
import useLocales from 'src/hooks/useLocales';
// layouts
import Layout from 'src/layouts';
// components
import Page from 'src/components/Page';
import {
  AppWidget,
  AppWelcome,
  AppNewInvoice,
  AppTopAuthors,
  AppTopRelated,
  AppAreaInstalled,
  AppWidgetSummary,
  AppCurrentDownload,
  AppTopInstalledCountries,
} from 'src/sections/@dashboard/general/app';

GeneralApp.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default function GeneralApp() {
  const theme = useTheme();

  const { user } = useAuth();
  const { themeStretch } = useSettings();
  const { translate } = useLocales();

  return (
    <Page title={translate('dashboard.app.title')}>
      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <AppWelcome displayName={user?.displayName} />
          </Grid>

          <Grid item xs={12} md={4}>
            <AppWidgetSummary
              percent={2.6}
              total={18765}
              chartColor={theme.palette.primary.main}
              title={translate('dashboard.app.totalActiveUsers')}
              chartData={[5, 18, 12, 51, 68, 11, 39, 37, 27, 20]}
            />
          </Grid>

          <Grid item xs={12} md={4}>
            <AppWidgetSummary
              percent={0.2}
              total={4876}
              chartColor={theme.palette.chart.blue[0]}
              title={translate('dashboard.app.totalInstalled')}
              chartData={[20, 41, 63, 33, 28, 35, 50, 46, 11, 26]}
            />
          </Grid>

          <Grid item xs={12} md={4}>
            <AppWidgetSummary
              percent={-0.1}
              total={678}
              chartColor={theme.palette.chart.red[0]}
              title={translate('dashboard.app.totalDownloads')}
              chartData={[8, 9, 31, 8, 16, 37, 8, 33, 46, 31]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppCurrentDownload />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppAreaInstalled />
          </Grid>

          <Grid item xs={12} lg={8}>
            <AppNewInvoice />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopRelated />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopInstalledCountries />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopAuthors />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <Stack spacing={1}>
              <AppWidget
                total={38566}
                chartData={48}
                icon={'eva:person-fill'}
                title={translate('dashboard.app.conversion')}
              />
              <AppWidget
                total={248}
                chartData={75}
                color="warning"
                icon={'eva:email-fill'}
                title={translate('dashboard.app.applications')}
              />
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}

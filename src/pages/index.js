import { useEffect } from 'react';
import { useRouter } from 'next/router';

import Page from 'src/components/Page';
import { PATH_DASHBOARD } from 'src/routes/paths';

export default function HomePage() {
  const { push } = useRouter();

  useEffect(() => {
    push(PATH_DASHBOARD.root);
  }, [push]);

  return <Page title="The starting point for your next project" />;
}

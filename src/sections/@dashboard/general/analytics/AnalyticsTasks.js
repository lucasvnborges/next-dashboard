import PropTypes from 'prop-types';
import { useState } from 'react';
// form
import { useForm, Controller } from 'react-hook-form';
// @mui
import { Card, Stack, Divider, Checkbox, MenuItem, IconButton, CardHeader, FormControlLabel } from '@mui/material';
// components
import Iconify from 'src/components/Iconify';
import MenuPopover from 'src/components/MenuPopover';

const TASKS = [
  'Criar Logo FireStone',
  'Adicionar arquivos SCSS e JS',
  'Reunião às 14:30',
  'Pesquisar novas tecnologias',
  'Revisar o Google Trends',
];

export default function AnalyticsTasks() {
  const { control } = useForm({
    defaultValues: {
      taskCompleted: ['Reunião às 14:30'],
    },
  });

  return (
    <Card>
      <CardHeader title="Tarefas" />

      <Controller
        name="taskCompleted"
        control={control}
        render={({ field }) => {
          const onSelected = (task) =>
            field.value.includes(task) ? field.value.filter((value) => value !== task) : [...field.value, task];

          return (
            <>
              {TASKS.map((task) => (
                <TaskItem
                  key={task}
                  task={task}
                  checked={field.value.includes(task)}
                  onChange={() => field.onChange(onSelected(task))}
                />
              ))}
            </>
          );
        }}
      />
    </Card>
  );
}

TaskItem.propTypes = {
  task: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};

function TaskItem({ task, checked, onChange }) {
  return (
    <Stack
      direction="row"
      sx={{
        px: 2,
        py: 0.75,
        ...(checked && {
          color: 'text.disabled',
          textDecoration: 'line-through',
        }),
      }}
    >
      <FormControlLabel
        control={<Checkbox checked={checked} onChange={onChange} />}
        label={task}
        sx={{ flexGrow: 1, m: 0 }}
      />
      <MoreMenuButton />
    </Stack>
  );
}

function MoreMenuButton() {
  const [open, setOpen] = useState(null);

  const handleOpen = (event) => {
    setOpen(event.currentTarget);
  };

  const handleClose = () => {
    setOpen(null);
  };

  const ICON = {
    mr: 2,
    width: 20,
    height: 20,
  };

  return (
    <>
      <IconButton size="large" onClick={handleOpen}>
        <Iconify icon={'eva:more-vertical-fill'} width={20} height={20} />
      </IconButton>

      <MenuPopover
        open={Boolean(open)}
        anchorEl={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        arrow="right-top"
        sx={{
          mt: -0.5,
          width: 'auto',
          '& .MuiMenuItem-root': { px: 1, typography: 'body2', borderRadius: 0.75 },
        }}
      >
        <MenuItem>
          <Iconify icon={'eva:checkmark-circle-2-fill'} sx={{ ...ICON }} />
          Completar
        </MenuItem>

        <MenuItem>
          <Iconify icon={'eva:edit-fill'} sx={{ ...ICON }} />
          Editar
        </MenuItem>

        <MenuItem>
          <Iconify icon={'eva:share-fill'} sx={{ ...ICON }} />
          Compartilhar
        </MenuItem>

        <Divider sx={{ borderStyle: 'dashed' }} />

        <MenuItem sx={{ color: 'error.main' }}>
          <Iconify icon={'eva:trash-2-outline'} sx={{ ...ICON }} />
          Excluir
        </MenuItem>
      </MenuPopover>
    </>
  );
}

import merge from 'lodash/merge';
import { useState } from 'react';
// @mui
import { Card, CardHeader, Box, TextField } from '@mui/material';
// hooks
import useLocales from 'src/hooks/useLocales';
// components
import ReactApexChart, { BaseOptionChart } from 'src/components/chart';

const CHART_DATA = [
  {
    year: 2019,
    data: [
      { name: 'Brasil', data: [10, 41, 35, 51, 49, 62, 69, 91, 148] },
      { name: 'Espanha', data: [10, 34, 13, 56, 77, 88, 99, 77, 45] },
    ],
  },
  {
    year: 2020,
    data: [
      { name: 'Brasil', data: [148, 91, 69, 62, 49, 51, 35, 41, 10] },
      { name: 'Espanha', data: [45, 77, 99, 88, 77, 56, 13, 34, 10] },
    ],
  },
];

const dates_en = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'];
const dates_pt = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set'];

export default function AppAreaInstalled() {
  const { translate, currentLang } = useLocales();

  const [seriesData, setSeriesData] = useState(2019);

  const handleChangeSeriesData = (event) => {
    setSeriesData(Number(event.target.value));
  };

  const chartOptions = merge(BaseOptionChart(), {
    xaxis: {
      categories: currentLang.value === 'pt' ? dates_pt : dates_en,
    },
  });

  return (
    <Card>
      <CardHeader
        title={translate('dashboard.app.areaInstalled')}
        subheader={`(+43%) ${translate('dashboard.app.thanLastYear')}`}
        action={
          <TextField
            select
            fullWidth
            value={seriesData}
            SelectProps={{ native: true }}
            onChange={handleChangeSeriesData}
            sx={{
              '& fieldset': { border: '0 !important' },
              '& select': {
                pl: 1,
                py: 0.5,
                pr: '24px !important',
                typography: 'subtitle2',
              },
              '& .MuiOutlinedInput-root': {
                borderRadius: 0.75,
                bgcolor: 'background.neutral',
              },
              '& .MuiNativeSelect-icon': {
                top: 4,
                right: 0,
                width: 20,
                height: 20,
              },
            }}
          >
            {CHART_DATA.map((option) => (
              <option key={option.year} value={option.year}>
                {option.year}
              </option>
            ))}
          </TextField>
        }
      />

      {CHART_DATA.map((item) => (
        <Box key={item.year} sx={{ mt: 3, mx: 3 }} dir="ltr">
          {item.year === seriesData && (
            <ReactApexChart type="line" series={item.data} options={chartOptions} height={364} />
          )}
        </Box>
      ))}
    </Card>
  );
}

import PropTypes from 'prop-types';
import orderBy from 'lodash/orderBy';
import { alpha, styled } from '@mui/material/styles';
import { Box, Stack, Card, Avatar, CardHeader, Typography } from '@mui/material';

import { fShortenNumber } from 'src/utils/formatNumber';
import { _appAuthors } from 'src/_mock';

import useLocales from 'src/hooks/useLocales';

import Iconify from 'src/components/Iconify';

const IconWrapperStyle = styled('div')(({ theme }) => ({
  width: 40,
  height: 40,
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.primary.main,
  backgroundColor: alpha(theme.palette.primary.main, 0.08),
}));

export default function AppTopAuthors() {
  const { translate } = useLocales();

  const displayAuthor = orderBy(_appAuthors, ['favourite'], ['desc']);

  return (
    <Card>
      <CardHeader title={translate('dashboard.app.topAuthors')} />
      <Stack spacing={3} sx={{ p: 3 }}>
        {displayAuthor.map((author, index) => (
          <AuthorItem key={author.id} author={author} index={index} />
        ))}
      </Stack>
    </Card>
  );
}

AuthorItem.propTypes = {
  author: PropTypes.shape({
    avatar: PropTypes.string,
    favourite: PropTypes.number,
    name: PropTypes.string,
  }),
  index: PropTypes.number,
};

function AuthorItem({ author, index }) {
  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <Avatar alt={author.name} src={author.avatar} />
      <Box sx={{ flexGrow: 1 }}>
        <Typography variant="subtitle2">{author.name}</Typography>
        <Typography
          variant="caption"
          sx={{
            mt: 0.5,
            display: 'flex',
            alignItems: 'center',
            color: 'text.secondary',
          }}
        >
          <Iconify icon={'eva:heart-fill'} sx={{ width: 16, height: 16, mr: 0.5 }} />
          {fShortenNumber(author.favourite)}
        </Typography>
      </Box>

      <IconWrapperStyle
        sx={{
          ...(index === 0 && {
            color: 'warning.main',
            bgcolor: (theme) => alpha(theme.palette.warning.main, 0.15),
          }),
          ...(index === 1 && {
            color: (theme) => alpha(theme.palette.grey[600], 1),
            bgcolor: (theme) => alpha(theme.palette.grey[600], 0.08),
          }),
          ...(index === 2 && {
            color: 'error.light',
            bgcolor: (theme) => alpha(theme.palette.error.light, 0.25),
          }),
        }}
      >
        <Iconify icon={'ant-design:trophy-filled'} width={20} height={20} />
      </IconWrapperStyle>
    </Stack>
  );
}

// @mui
import { styled } from '@mui/material/styles';
import { Button, Card, Typography, Stack } from '@mui/material';
// utils
import { fCurrency } from 'src/utils/formatNumber';
// hooks
import useLocales from 'src/hooks/useLocales';

const RowStyle = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
});

export default function EcommerceCurrentBalance() {
  const { translate } = useLocales();

  const currentBalance = 18765;
  const sentAmount = 2550;
  const pending = 1850;
  const totalAmount = (currentBalance + pending - sentAmount).toFixed(0);

  return (
    <Card sx={{ p: 3 }}>
      <Typography variant="subtitle2" gutterBottom>
        {translate('ecommerce.currentBalance')}
      </Typography>

      <Stack spacing={2}>
        <Typography variant="h3">{fCurrency(currentBalance)}</Typography>

        <RowStyle>
          <Typography variant="body2" sx={{ color: 'text.secondary' }}>
            {translate('ecommerce.pending')}
          </Typography>
          <Typography variant="body2">{fCurrency(pending)}</Typography>
        </RowStyle>

        <RowStyle>
          <Typography variant="body2" sx={{ color: 'text.secondary' }}>
            {translate('ecommerce.sentAmount')}
          </Typography>
          <Typography variant="body2">- {fCurrency(sentAmount)}</Typography>
        </RowStyle>

        <RowStyle>
          <Typography variant="body2" sx={{ color: 'text.secondary' }}>
            {translate('ecommerce.totalAmount')}
          </Typography>
          <Typography variant="subtitle1">{fCurrency(totalAmount)}</Typography>
        </RowStyle>

        <Stack direction="row" spacing={1.5}>
          <Button fullWidth variant="outlined">
            {translate('ecommerce.receive')}
          </Button>
          <Button fullWidth variant="contained" color="warning">
            {translate('ecommerce.transfer')}
          </Button>
        </Stack>
      </Stack>
    </Card>
  );
}

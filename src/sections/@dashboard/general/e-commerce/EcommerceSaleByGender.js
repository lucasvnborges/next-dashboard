import merge from 'lodash/merge';
// @mui
import { useTheme, styled } from '@mui/material/styles';
import { Card, CardHeader } from '@mui/material';
// utils
import { fNumber } from 'src/utils/formatNumber';
// hooks
import useLocales from 'src/hooks/useLocales';
// components
import ReactApexChart, { BaseOptionChart } from 'src/components/chart';

const CHART_HEIGHT = 392;
const LEGEND_HEIGHT = 72;

const ChartWrapperStyle = styled('div')(({ theme }) => ({
  height: CHART_HEIGHT,
  marginTop: theme.spacing(2),
  '& .apexcharts-canvas svg': { height: CHART_HEIGHT },
  '& .apexcharts-canvas svg,.apexcharts-canvas foreignObject': {
    overflow: 'visible',
  },
  '& .apexcharts-legend': {
    height: LEGEND_HEIGHT,
    alignContent: 'center',
    position: 'relative !important',
    borderTop: `solid 1px ${theme.palette.divider}`,
    top: `calc(${CHART_HEIGHT - LEGEND_HEIGHT}px) !important`,
  },
}));

const CHART_DATA = [41, 41, 16];

export default function EcommerceSaleByGender() {
  const theme = useTheme();

  const { translate } = useLocales();

  const chartOptions = merge(BaseOptionChart(), {
    labels: ['Homens', 'Mulheres', 'Não definido'],
    legend: { floating: true, horizontalAlign: 'center' },
    fill: {
      type: 'gradient',
      gradient: {
        colorStops: [
          [
            {
              offset: 0,
              color: theme.palette.primary.light,
            },
            {
              offset: 100,
              color: theme.palette.primary.main,
            },
          ],
          [
            {
              offset: 0,
              color: theme.palette.warning.light,
            },
            {
              offset: 100,
              color: theme.palette.warning.main,
            },
          ],
          [
            {
              offset: 0,
              color: theme.palette.info.light,
            },
            {
              offset: 100,
              color: theme.palette.info.main,
            },
          ],
        ],
      },
    },
    plotOptions: {
      radialBar: {
        hollow: { size: '62%' },
        dataLabels: {
          value: { offsetY: 16 },
          total: {
            formatter: () => fNumber(765),
          },
        },
      },
    },
  });

  return (
    <Card>
      <CardHeader title={translate('ecommerce.saleByGender')} />
      <ChartWrapperStyle dir="ltr">
        <ReactApexChart type="radialBar" series={CHART_DATA} options={chartOptions} height={310} />
      </ChartWrapperStyle>
    </Card>
  );
}

import PropTypes from 'prop-types';
// @mui
import { styled } from '@mui/material/styles';
import { Link, Card, Typography, CardHeader, Stack } from '@mui/material';
// components
import Iconify from 'src/components/Iconify';

const IconStyle = styled(Iconify)(({ theme }) => ({
  width: 20,
  height: 20,
  marginTop: 1,
  flexShrink: 0,
  marginRight: theme.spacing(2),
}));

ProfileAbout.propTypes = {
  profile: PropTypes.object,
};

export default function ProfileAbout({ profile }) {
  const { quote } = profile;

  return (
    <Card>
      <CardHeader title="Sobre" />

      <Stack spacing={2} sx={{ p: 3 }}>
        <Typography variant="body2">{quote}</Typography>

        <Stack direction="row">
          <IconStyle icon={'eva:pin-fill'} />
          <Typography variant="body2">
            <Link component="span" variant="subtitle2" color="text.primary" style={{ cursor: 'pointer' }}>
              Brazil
            </Link>
          </Typography>
        </Stack>

        <Stack direction="row">
          <IconStyle icon={'eva:email-fill'} />
          <Typography variant="body2">Lucasvnborges@gmail.com</Typography>
        </Stack>

        <Stack direction="row">
          <IconStyle icon={'ic:round-business-center'} />
          <Typography variant="body2">
            Fundador da&nbsp;
            <Link component="span" variant="subtitle2" color="text.primary">
              Hapbit
            </Link>
          </Typography>
        </Stack>

        <Stack direction="row">
          <IconStyle icon={'ic:round-business-center'} />
          <Typography variant="body2">
            Estudante no&nbsp;
            <Link component="span" variant="subtitle2" color="text.primary" style={{ cursor: 'pointer' }}>
              https://egghead.io
            </Link>
          </Typography>
        </Stack>
      </Stack>
    </Card>
  );
}

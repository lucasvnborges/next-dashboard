<br>

### <h3 align="center">[🌐 https://nxdash.app](https://nxdash.netlify.app)</h3>

<br><br>

<div align="center">
    <img src="./public/screenshot_1.png">
</div>

## 📦 &nbsp; Tecnologias

- Next.js https://nextjs.org
- Redux https://redux.js.org
- Firebase https://firebase.google.com
- Emotion https://emotion.sh/docs/introduction
- Yup https://www.npmjs.com/package/yup
